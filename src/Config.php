<?php


namespace Sharecoto;

class Config
{
    protected $env;
    protected $path;

    public function __construct($env = 'development', $path)
    {
        $this->env = $env;
        $this->path = $path;
    }

    public function __get($name)
    {
        if (!empty($this->$name)) {
            return $this->$name;
        }
        return $this->getAppConfig($name);
    }

    /**
     * $nameに対するコンフィグを配列で返す。
     * コンフィグファイルが見つからない場合はエラーにならずに空の配列を返す
     *
     * @param string $name
     * @return array
     */
    public function getAppConfig($name)
    {
        $confFile = $this->path . '/' . $name . '.php';
        $envConfFile = $this->path . '/' . $this->env .'/' . $name . '.php';

        $config = array();
        if (file_exists($confFile)) {
            $config = require($confFile);
        }

        $envConfig = array();
        if (file_exists($envConfFile)) {
            $envConfig = require($envConfFile);
        }

        return array_merge($config, $envConfig);
    }
}
